;;; Commentary:
;;   - All of the configuration is within `configuration.org`
;;; Code:
(require 'org)
(package-initialize)

(cond ((eq system-type 'windows-nt)
       ;;;windows code here
       (defvar my-compiled-lisp-file "C:/Users/glagerwall/AppData/Roaming/.emacs.d/configuration.elc"
         "All configurations stored in this file.")
       (defvar my-lisp-file "C:/Users/glagerwall/AppData/Roaming/.emacs.d/configuration.el"
         "All configurations stored in this file.")
       (defvar my-org-file "C:/Users/glagerwall/AppData/Roaming/.emacs.d/configuration.org"
         "All configurations tangled from this file.")
       )
      ((eq system-type 'gnu/linux)
       ;;;linux code here
       (defvar my-compiled-lisp-file "~/.emacs.d/configuration.elc"
         "All configurations stored in this file.")
       (defvar my-lisp-file "~/.emacs.d/configuration.el"
         "All configurations stored in this file.")
       (defvar my-org-file "~/.emacs.d/configuration.org"
         "All configurations tangled from this file.")
       )
      )

;; This loads the actual configuration in literate org-mode elisp
;(defun load-config()
;  (interactive)
;  (org-babel-load-file "C:/Users/Lagerwall/AppData/Roaming/.emacs.d/configuration.org"))
;;  (org-babel-load-file my-org-file))
;(load-config)

;; slightly different implementation, checking if .elc exists first
;(if (file-exists-p my-init-file)
;    (load-file my-init-file)
;  (progn
;    (org-babel-load-file my-org-file)))

;; My code.
;; Check if compiled (.elc) exists. Then check if it is newer than .org. If not, tangle, compile, and then load.
(if (file-exists-p my-compiled-lisp-file)
    (if (file-newer-than-file-p my-compiled-lisp-file my-org-file)
      (progn (load-file my-lisp-file)
             (message "Loaded Existing Byte-Compiled Configuration File"))
      (progn (org-babel-tangle-file my-org-file)
             (byte-compile-file my-lisp-file)
             (load-file my-lisp-file)
             (message "Old File Exists. Tangled Org File, Before Compiling, Then Loading")))
  (progn (org-babel-tangle-file my-org-file)
         (byte-compile-file my-lisp-file)
         (load-file my-lisp-file)
         (message "Tangled Org File, Before Compiling, Then Loading")))
;;; init.el ends here
 
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files nil)
 '(use-package-always-defer t)
 '(use-package-always-ensure t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
